var map;
var marker;

function initMap() {
    let mapDiv = $('#map');
    if (mapDiv.length > 0) {
        marker = new google.maps.Marker({
            position: {lat: 0, lng: 0},
            map: null
        });

        if (mapDiv.attr('data-latitude') === undefined || mapDiv.attr('data-longitude') === undefined) {
            drawUserLocation();

        } else {
            let position = {
                lat: parseFloat(mapDiv.attr('data-latitude')),
                lng: parseFloat(mapDiv.attr('data-longitude'))
            };
            displayMap(position, true);
        }
    }

}

function drawUserLocation() {
    let position = {lat: 0, lng: 0};

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (receivedPosition) {
            position.lat = receivedPosition.coords.latitude;
            position.lng = receivedPosition.coords.longitude;
            displayMap(position, false);
        });
    } else {
        displayMap(position, false);
    }

}

function displayMap(position, drawMarker) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: position,
        zoom: 14
    });

    if (drawMarker) {
        marker = new google.maps.Marker({position: position, map: map});
    } else {
        map.addListener('click', function (e) {
            placeMarkerAndPanTo(e.latLng, map);
        });
    }
}

function placeMarkerAndPanTo(position, map) {
    marker.setMap(null);
    marker = new google.maps.Marker({
        position: position,
        map: map
    });
    map.panTo(position);
}

function getMarkerPosition() {
    if (marker.getMap() !== null) {
        return {
            lat: marker.position.lat(),
            lng: marker.position.lng()
        };
    }
    return null;
}

$(function () {
    $('.history-marker').on('click', function (event) {
        if (map === undefined || map === null) {
            return;
        }
        let element = $(event.currentTarget);
        element.closest('ul').find('li').removeClass('selected');
        element.addClass('selected');
        let position = {
            lat: parseFloat(element.attr('data-latitude')),
            lng: parseFloat(element.attr('data-longitude'))
        };
        placeMarkerAndPanTo(position, map);
    });
});
