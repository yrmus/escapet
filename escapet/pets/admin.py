from django.contrib import admin

from .models import Pet, PetCategory, PetPosition
# Register your models here.

admin.site.register(Pet)
admin.site.register(PetCategory)
admin.site.register(PetPosition)
