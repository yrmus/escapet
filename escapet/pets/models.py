from datetime import datetime
from django.db import models
from django.utils.crypto import get_random_string


class PetCategory(models.Model):
    name = models.CharField(max_length=100)


class Pet(models.Model):
    input_date = models.DateTimeField(default=datetime.now)
    title = models.CharField(max_length=100)
    description = models.TextField()
    category = models.ForeignKey(PetCategory, on_delete=models.CASCADE)
    phone = models.CharField(max_length=20)
    name = models.CharField(max_length=50)
    token = models.CharField(max_length=64, default=get_random_string(length=64))
    image = models.ImageField(upload_to='static/images/uploads', default='static/images/nophoto.png')
    email = models.EmailField()
    taken = models.BooleanField(default=False)


class PetPosition(models.Model):
    input_date = models.DateTimeField(default=datetime.now)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
