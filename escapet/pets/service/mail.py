from django.core.mail import send_mail
from django.shortcuts import get_object_or_404
from django.template.loader import get_template, render_to_string
from django.urls import reverse
from ..models import Pet


class MailService:

    @staticmethod
    def send_create_pet(pet_id, receiver_email, request):
        pet = get_object_or_404(Pet, id=pet_id)
        owner_url = request.build_absolute_uri(reverse('pets:details_owner', kwargs={'pk': pet_id, 'token': pet.token}))
        context = {
            'pet': pet,
            'pet_url': owner_url
        }
        send_mail(
            'Thank you for registering new pet',
            'New pet added',
            'noreply@escapet.penguit.net',
            [receiver_email],
            fail_silently=False,
            html_message=render_to_string('mail/new_pet.html', context),

        )
