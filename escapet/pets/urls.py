from django.urls import path

from . import views

app_name = 'pets'
urlpatterns = [
    path('', views.index, name='pet_list'),
    path('new/', views.NewView.as_view(), name='add_pet'),
    path('pet/<int:pk>', views.detail_view, name='details'),
    path('pet/<int:pet_id>/edit/<slug:token>', views.edit, name='details_owner'),
    path('pet/<int:pet_id>/position', views.new_position, name='new_position'),
    path('pet/<int:pet_id>/catch', views.catch_pet, name='catch'),
]