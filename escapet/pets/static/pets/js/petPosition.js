$(function () {
    let form = $('form');
    if (form.length === 0) {
        return;
    }
    form.on('submit', function (event) {
        let latInput = form.find('input[data-latitude_input]');
        let lngInput = form.find('input[data-longitude_input]');
        if (latInput.length > 0 && lngInput.length > 0) {
            if (getMarkerPosition() === null) {
                alert('Please specify Pet\'s position');
                return false;
            }
            let position = getMarkerPosition();
            latInput.val(position.lat.toFixed(6));
            lngInput.val(position.lng.toFixed(6));
        }
    })
});