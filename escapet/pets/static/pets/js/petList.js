$(function () {


    $('#category-combo').on('change', function (event) {
        updatePosition(event);
        // $(event.currentTarget).closest('form').submit();
    });
    $('#taken-combo').on('change', function (event) {
        updatePosition(event);
        // $(event.currentTarget).closest('form').submit();
    });
    $('#radius-combo').on('change', function (event) {
        updatePosition(event);
        // $(event.currentTarget).closest('form').submit();
    });

    function updatePosition(event) {
        console.log('updated');
        let position = { lat: 0, lng: 0 };
        let form = $(event.currentTarget).closest('form');

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (receivedPosition) {
                console.log('recieved', receivedPosition);
                position.lat = receivedPosition.coords.latitude;
                position.lng = receivedPosition.coords.longitude;

                let latInput = form.find('input[data-latitude_input]');
                let lngInput = form.find('input[data-longitude_input]');
                latInput.val(position.lat.toFixed(6));
                lngInput.val(position.lng.toFixed(6));
                form.submit();
            });
        } else {
            form.submit();
        }

    }
});