# Generated by Django 2.1.2 on 2018-11-03 20:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pets', '0010_auto_20181103_2032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pet',
            name='image',
            field=models.ImageField(default='static/images/nophoto.png', upload_to='static/images/uploads'),
        ),
    ]
