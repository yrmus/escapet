# Generated by Django 2.1.2 on 2018-11-03 19:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pets', '0004_pet_input_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='pet',
            name='image',
            field=models.FileField(default='settings.BASE_DIR/static/images/nophoto.jpg', upload_to='uploads'),
        ),
    ]
