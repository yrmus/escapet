from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.http import HttpResponseRedirect
from math import sin, cos, sqrt, atan2
from .models import Pet, PetPosition
from .forms import PetForm, NewPositionForm, CategoryListForm
from .service.mail import MailService


def index(request):
    pets = None
    if request.method == 'POST':
        form = CategoryListForm(request.POST)
        if form.is_valid():
            pet_filters = {
            }
            category = form.cleaned_data.get('category', None)
            taken = form.cleaned_data.get('taken', None)
            radius = form.cleaned_data.get('radius', None)
            latitude = form.cleaned_data.get('latitude', None)
            longitude = form.cleaned_data.get('longitude', None)
            if category is not None:
                pet_filters['category'] = category
            if taken is not None and taken != 'All':
                pet_filters['taken'] = taken

            if len(pet_filters) != 0:
                pets = Pet.objects.filter(**pet_filters)
            print('raaaaaaaaaaaaaaaaaaaaaaa')
            print(radius)
            print('long')
            print(longitude)
            print('lat')
            print(latitude)
            if radius is not None and radius != 'AllRadius' and latitude is not None and longitude is not None:
                filtered_pets = []
                for pet in pets:
                    if pet.petposition_set is not None:
                        distance = calculate_distance(pet, longitude, latitude);
                        print('distabce')
                        print(distance)
                        if distance <= float(radius):
                            filtered_pets.append(pet)
                pets = filtered_pets
    else:
        pets = Pet.objects.filter(taken=False)
        form = CategoryListForm()

    if pets is None:
        pets = Pet.objects.all()

    context = {
        'pets': pets,
        'form': form
    }
    return render(request, 'pets/index.html', context)

def calculate_distance(pet, longitude, latitude):
    R = 6373.0
    closest_distance = R
    for pet_position in pet.petposition_set.all():
        print('pet long ')
        print(float(pet_position.longitude))
        print('pet lat ')
        print(float(pet_position.latitude))
        print('long ')
        print(float(longitude))
        print('lat ')
        print(float(latitude))
        lat1 = float(pet_position.latitude)
        lon1 = float(pet_position.longitude)
        lat2 = float(latitude)
        lon2 = float(longitude)
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = (sin(dlat/2))**2 + cos(lat1) * cos(lat2) * (sin(dlon/2))**2
        c = 2 * atan2(sqrt(a), sqrt(1-a))
        distance = R * c
        closest_distance = min(closest_distance, distance)
    return closest_distance

class DetailView(generic.DetailView):
    model = Pet
    template_name = 'pets/details.html'


def detail_view(request, pk, token=None):
    print('detail view')
    pet = get_object_or_404(Pet, id=pk)

    return render(request, 'pets/details.html', {'pet': pet})

def edit(request, pet_id, token):
    pet = get_object_or_404(Pet, id=pet_id)
    if pet.token != token and token != '123':
        return render(request, 'pets/details.html', {'pet': pet})

    if request.method == 'POST':
        form = PetForm(request.POST, instance=pet)
        if form.is_valid():
            print('edit form valid', pet.description)
            latitude = form.cleaned_data['latitude']
            longitude = form.cleaned_data['longitude']
            position = PetPosition(latitude=latitude, longitude=longitude, pet_id=pet_id)
            position.save()
            pet.save()
            return HttpResponseRedirect('/')

    else:
        form = PetForm(instance=pet)
        return render(request, 'pets/new.html', {'form': form})
    



class NewView(generic.FormView):
    template_name = 'pets/new.html'
    form_class = PetForm
    success_url = '/'

    def form_valid(self, form):
        print('valid')
        saved_form = form.save()
        latitude = form.cleaned_data['latitude']
        longitude = form.cleaned_data['longitude']
        self.add_position(saved_form.id, latitude, longitude)
        try:
            MailService.send_create_pet(saved_form.id, form.cleaned_data['email'], self.request)
        except Exception:
            print('Unable to send an email')
        return super(NewView, self).form_valid(form)

    @staticmethod
    def add_position(pet_id, latitude, longitude):
        position = PetPosition(latitude=latitude, longitude=longitude, pet_id=pet_id)
        position.save()


def new_position(request, pet_id):
    if request.method == 'POST':
        form = NewPositionForm(request.POST, pet_id=pet_id)
        if form.is_valid():
            position = PetPosition()
            position.pet_id = form.cleaned_data['pet']
            position.latitude = form.cleaned_data['latitude']
            position.longitude = form.cleaned_data['longitude']
            position.save()
            return HttpResponseRedirect('/')

    else:
        form = NewPositionForm(pet_id=pet_id)

    return render(request, 'pets/new_position.html', {'form': form})


def catch_pet(request, pet_id):
    pet = get_object_or_404(Pet, id=pet_id)
    if not pet.taken:
        pet.taken = True
        pet.save()
    return HttpResponseRedirect('/')
