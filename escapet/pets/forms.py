from django import forms
from django.forms import Textarea
from .models import Pet, PetPosition, PetCategory


class CategoryChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class CategoryListForm(forms.Form):
    taken_choices = (
        ('False', 'Not taken'),
        ('True', 'Taken'),
        ('All', 'All'),
    )
    radius_choices = (
        ('AllRadius', 'Whole world'),
        (5, '5km'),
        (10, '10km'),
    )
    category = CategoryChoiceField(queryset=PetCategory.objects.all(), label='', empty_label='All categories',
                                   widget=forms.Select(attrs={'id': 'category-combo'}), required=False)
    taken = forms.ChoiceField(choices=taken_choices, required=False, label='',
                              widget=forms.Select(attrs={'id': 'taken-combo'}), initial=False)
    radius = forms.ChoiceField(choices=radius_choices, required=False, label='',
                              widget=forms.Select(attrs={'id': 'radius-combo'}), initial=False)
    latitude = forms.CharField(widget=forms.HiddenInput(attrs={'data-latitude_input': True}))
    longitude = forms.CharField(widget=forms.HiddenInput(attrs={'data-longitude_input': True}))


class PetForm(forms.ModelForm):
    latitude = forms.CharField(widget=forms.HiddenInput(attrs={'data-latitude_input': True}), initial='10')
    longitude = forms.CharField(widget=forms.HiddenInput(attrs={'data-longitude_input': True}), initial='10')
    category = CategoryChoiceField(queryset=PetCategory.objects.all())

    class Meta:
        model = Pet
        fields = ('title', 'description', 'email', 'phone', 'category', 'image')
        widgets = {
            'description': Textarea(attrs={'rows': 5})
        }


class NewPositionForm(forms.Form):
    pet = forms.CharField(widget=forms.HiddenInput())
    latitude = forms.CharField(widget=forms.HiddenInput(attrs={'data-latitude_input': True}))
    longitude = forms.CharField(widget=forms.HiddenInput(attrs={'data-longitude_input': True}))

    def __init__(self, *args, **kwargs):
        pet_id = kwargs.pop('pet_id')
        super(NewPositionForm, self).__init__(*args, **kwargs)
        self.fields['pet'] = forms.CharField(widget=forms.HiddenInput(), initial=pet_id)

    class Meta:
        model = PetPosition
        fields = {'latitude', 'longitude', 'pet'}
